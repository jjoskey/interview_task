from common.db.basic import BaseModel, EnumField
import peewee
from application.models.enums import OperationTypeEnum


class Client(BaseModel):
    email = peewee.CharField(unique=True)


class Wallet(BaseModel):
    client = peewee.ForeignKeyField(
        Client, on_delete='CASCADE', backref='wallet', unique=True)


class Operation(BaseModel):
    to_wallet = peewee.ForeignKeyField(
        Wallet, backref='incoming_operations', index=True)
    from_wallet = peewee.ForeignKeyField(
        Wallet, backref='outgoing_operations', null=True, index=True)
    amount = peewee.DecimalField(max_digits=20, decimal_places=2)
    type_ = EnumField(OperationTypeEnum)
    request_uid = peewee.CharField(max_length=36, unique=True)
