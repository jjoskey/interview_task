from decimal import Decimal

from pydantic import BaseModel, Field


class ErrorResponse(BaseModel):
    message: str = Field(..., title='Error message', max_length=500)
    error_code: str = Field(..., title='Error code')


class CreateClientResponse(BaseModel):
    client_uid: str = Field(..., title='Client UUID')
    wallet_uid: str = Field(..., title='Wallet UUID')


class BalanceResponse(BaseModel):
    wallet_uid: str = Field(..., title='Wallet UUID')
    balance: Decimal = Field(..., title='Balance of Wallet')


class OperationResponse(BaseModel):
    wallet_uid: str = Field(..., title='Wallet UUID')
    balance: Decimal = Field(..., title='Balance of Wallet')
    request_uid: str = Field(..., title='Request UUID')
