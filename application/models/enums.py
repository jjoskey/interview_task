from enum import Enum, auto


class OperationTypeEnum(Enum):
    deposit = auto()
    transfer = auto()


class StatusEnum(Enum):
    success = 200
    bad_request = 400
    not_found = 404
    server_error = 500


class ErrorCodeEnum(Enum):
    internal_error = 'Internal Error'
    validation_error = 'Validation Error'
    does_not_exist = 'Does Not Exist'
    database_error = 'Database Error'
    not_enough_money = 'Not Enough Money'
    request_uid_not_unique = 'Request UUID not unique'
