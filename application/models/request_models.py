from decimal import Decimal

from email_validator import validate_email
from pydantic import BaseModel, validator, ValidationError, Field, root_validator
from pydantic.error_wrappers import ErrorWrapper
from uuid import UUID


class AmountMixin(BaseModel):
    amount: Decimal = Field(..., title='Amount of deposit, USD')

    @validator('amount')
    def validate_amount(cls, value):
        after_dot = ''
        if len(str(value).split('.')) == 2:
            after_dot = str(value).split('.')[1]
        if value > 0 and len(str(value)) <= 20 and len(after_dot) <= 2:
            return value
        raise ValidationError(
            [ErrorWrapper(Exception('Amount should be positive and consists 2 or less digits after dot'),
                          loc='amount')],
            cls.__class__
        )


class IdempotencyMixin(BaseModel):
    request_uid: str = Field(..., title='Request ID')

    @validator('request_uid')
    def validate_request_uid(cls, value):
        try:
            return str(UUID(value))
        except ValueError:
            raise ValidationError(
                [ErrorWrapper(Exception('requist_uid should be UUID-like string'),
                              loc='amount')],
                cls.__class__
            )


class CreateClientRequest(BaseModel):
    email: str = Field(..., title='Email')

    @validator('email')
    def validate_email_(cls, value):
        return validate_email(value, check_deliverability=False).email


class DepositRequest(AmountMixin, IdempotencyMixin, BaseModel):
    wallet_uid: str = Field(..., title='Wallet UUID')


class TransferRequest(AmountMixin, IdempotencyMixin, BaseModel):
    to_wallet_uid: str = Field(..., title='Wallet UUID of Payee')
    from_wallet_uid: str = Field(..., title='Wallet UUID of Payer')

    @root_validator
    def validate_wallets_are_different(cls, values):
        if values['to_wallet_uid'] != values['from_wallet_uid']:
            return values
        raise ValidationError(
            [ErrorWrapper(Exception(
                'Transfer can only be carried out with different wallets'), loc='wallets')],
            cls.__class__
        )
