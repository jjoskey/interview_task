from aiohttp import web

from application.models.db_models import Wallet
from application.models.request_models import DepositRequest, TransferRequest
from application.models.response_models import OperationResponse, BalanceResponse
from application.services.balance import BalanceController
from application.services.deposit import DepositController
from application.services.transfer import TransferController
from common.db.basic import manager


class DepositView(web.View):
    async def post(self) -> OperationResponse:
        request_data = await self.request.json()
        validated_data = DepositRequest(**request_data)

        new_balance = await DepositController(**validated_data.dict()).make_deposit()

        return OperationResponse(wallet_uid=validated_data.wallet_uid, balance=new_balance,
                                 request_uid=validated_data.request_uid)


class TransferView(web.View):
    async def post(self) -> OperationResponse:
        request_data = await self.request.json()
        validated_data = TransferRequest(**request_data)

        new_balance = await TransferController(**validated_data.dict()).make_transfer()

        return OperationResponse(wallet_uid=validated_data.from_wallet_uid, balance=new_balance,
                                 request_uid=validated_data.request_uid)


class BalanceView(web.View):
    async def get(self) -> BalanceResponse:
        wallet_uid = self.request.match_info.get('wallet_uid')
        wallet = await manager.get(Wallet, uid=wallet_uid)
        balance = await BalanceController(wallet).get_balance()
        return BalanceResponse(wallet_uid=wallet_uid, balance=balance)
