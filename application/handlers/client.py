from aiohttp import web

from application.models.db_models import Client, Wallet
from application.models.request_models import CreateClientRequest
from application.models.response_models import CreateClientResponse
from common.db.basic import manager


class ClientView(web.View):
    async def post(self) -> CreateClientResponse:
        request_data = await self.request.json()
        validated_data = CreateClientRequest(**request_data)
        client = await manager.create(Client, email=validated_data.email)
        wallet = await manager.create(Wallet, client=client)
        return CreateClientResponse(client_uid=client.uid, wallet_uid=wallet.uid)
