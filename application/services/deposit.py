from decimal import Decimal

import peewee_async
from peewee import DoesNotExist

from application.models.db_models import Wallet, Operation
from application.models.enums import OperationTypeEnum
from application.services.balance import BalanceController
from common.db.basic import manager, database
from common.errors import BadRequestUidError


class DepositController:
    def __init__(self, wallet_uid: str, amount: Decimal, request_uid: str):
        self.wallet_uid = wallet_uid
        self.amount = amount
        self.request_uid = request_uid

    async def make_deposit(self) -> Decimal:
        async with peewee_async.atomic(database):
            try:
                wallet = (await manager.execute(Wallet.select().where(
                    Wallet.uid == self.wallet_uid).for_update()))[0]
            except IndexError:
                raise DoesNotExist

            try:
                await manager.get(Operation, request_uid=self.request_uid)
                raise BadRequestUidError
            except DoesNotExist:
                pass

            await manager.create(
                Operation, to_wallet=wallet, amount=self.amount, type_=OperationTypeEnum.deposit,
                request_uid=self.request_uid)

        new_balance = await BalanceController(wallet).get_balance_from_operations()
        return new_balance
