from decimal import Decimal

import peewee_async
from peewee import DoesNotExist

from application.models.db_models import Wallet, Operation
from application.models.enums import OperationTypeEnum
from application.services.balance import BalanceController
from common.db.basic import manager, database
from common.errors import NotEnoughMoneyError, BadRequestUidError


class TransferController:
    def __init__(self, to_wallet_uid: str, from_wallet_uid: str, amount: Decimal, request_uid: str):
        self.to_wallet_uid = to_wallet_uid
        self.from_wallet_uid = from_wallet_uid
        self.amount = amount
        self.request_uid = request_uid

    async def make_transfer(self) -> Decimal:
        async with peewee_async.atomic(database):
            wallets = await manager.execute(Wallet.select().where(
                Wallet.uid.in_([self.from_wallet_uid, self.to_wallet_uid])).for_update())

            try:
                if wallets[0].uid == self.from_wallet_uid:
                    from_wallet, to_wallet = wallets[0], wallets[1]
                else:
                    from_wallet, to_wallet = wallets[1], wallets[0]
            except IndexError:
                raise DoesNotExist

            try:
                await manager.get(Operation, request_uid=self.request_uid)
                raise BadRequestUidError
            except DoesNotExist:
                pass

            from_balance_controller = BalanceController(from_wallet)
            if not await self._can_transfer(from_balance_controller):
                raise NotEnoughMoneyError
            await manager.create(
                Operation, from_wallet=from_wallet, to_wallet=to_wallet, amount=self.amount,
                type_=OperationTypeEnum.transfer, request_uid=self.request_uid
            )

            to_balance_controller = BalanceController(to_wallet)
            await to_balance_controller.get_balance_from_operations()
            from_old_balance = await from_balance_controller.get_balance()
            from_new_balance = from_old_balance - self.amount
            await from_balance_controller.set_balance(from_new_balance)

        return from_new_balance

    async def _can_transfer(self, from_balance_controller: BalanceController) -> bool:
        balance = await from_balance_controller.get_balance_from_operations()
        return self.amount <= balance
