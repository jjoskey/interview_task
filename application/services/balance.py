from decimal import Decimal
from typing import Optional, Union

from aioredis import Redis
from peewee import fn

import config
from application.models.db_models import Wallet, Operation
from application.models.enums import OperationTypeEnum
from common.cache.basic import Cache
from common.db.basic import manager


class BalanceController:
    def __init__(self, wallet: Wallet, connect: Optional[Redis] = None):
        self.wallet = wallet
        self._cache = Cache(connect)

    async def get_balance_from_operations(self) -> Decimal:
        subquery = Operation \
            .select(fn.SUM(Operation.amount).alias('summary')) \
            .where(Operation.to_wallet == self.wallet,
                   Operation.type_.in_([OperationTypeEnum.deposit, OperationTypeEnum.transfer])) | \
            Operation \
            .select((fn.SUM(Operation.amount) * -1).alias('summary')) \
            .where(Operation.from_wallet == self.wallet,
                   Operation.type_ == OperationTypeEnum.transfer)
        query = Operation.select(
            fn.SUM(subquery.c.summary).alias('balance')).from_(subquery)
        result = await manager.execute(query)

        balance = result[0].balance or 0
        await self.set_balance(balance)
        return balance

    async def get_balance(self) -> Decimal:
        from_cache = await self._cache.get(self._key)
        return Decimal(from_cache) if from_cache is not None else await self.get_balance_from_operations()

    async def set_balance(self, value: Union[int, Decimal]) -> bool:
        return await self._cache.set(self._key, str(value))

    @property
    def _key(self):
        return f'{config.BALANCE_CACHE_PREFIX}{self.wallet.uid}'
