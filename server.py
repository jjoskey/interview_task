from aiohttp import web

import config
from application.handlers.client import ClientView
from application.handlers.payment import DepositView, TransferView, BalanceView
from common.cache import basic as redis
from common.db import basic as db
from common.middlewares import create_rest_response, set_context


async def start_background_tasks(application: web.Application) -> None:
    application['db'] = db.start()
    application['redis'] = await redis.start()


async def cleanup_background_tasks(application: web.Application) -> None:
    await application['db'].close()
    application['redis'].close()
    await application['redis'].wait_closed()


def create_app() -> web.Application:
    app = web.Application()
    app.router.add_routes([
        web.route('POST', '/client', ClientView),
        web.route('POST', '/deposit', DepositView),
        web.route('POST', '/transfer', TransferView),
        web.route('GET', '/balance/{wallet_uid}', BalanceView)
    ])
    app.middlewares.append(create_rest_response)
    app.middlewares.append(set_context)
    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)

    return app


def init() -> None:
    app = create_app()
    web.run_app(app=app, host=config.HOST, port=config.PORT,
                reuse_port=True, shutdown_timeout=60)


if __name__ == '__main__':
    init()
