DB_NAME = 'payment'
DB_USER = 'payment'
DB_PASS = 'payment'
DB_HOST = 'localhost'
DB_PORT = 5432

HOST = '0.0.0.0'
PORT = '8080'

REDIS_HOST = 'localhost'
REDIS_PORT = 6379

DEFAULT_EXPIRE = 3600
BALANCE_CACHE_PREFIX = 'balance_'
