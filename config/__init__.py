from config.base import *  # noqa
import ast
import os

# Override config variables from environment
for var in list(locals()):
    value = os.getenv(var)
    if value is None:
        continue
    try:
        locals()[var] = ast.literal_eval(value)
    except:   # noqa
        locals()[var] = value
