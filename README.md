Проект поднимается с помощью команды: 
```sh
docker-compose up
```
Также можно запустить контейнер с тестами:
```sh
docker-compose -f docker-compose-test.yml up --build  --abort-on-container-exit
```
Проект будет доступен на 

```sh
http://0.0.0.0:8080
```

Ендпоинты:

```sh
POST /client - создание клиента с кошельком
POST /deposit - зачисление денег на кошелек 
POST /transfer - перевод с одного кошелька на другой
GET /balance - баланс кошелька 
```

Код с юрлами и запросами:

```python
# pip install requests
# pip install faker

from decimal import Decimal

import requests
from faker import Faker

URL = 'http://0.0.0.0:8080'


create_client_response = requests.post(f'{URL}/client', json={'email': Faker().email()})
# создаем кошелек
wallet_uid = create_client_response.json()['wallet_uid']

another_wallet_uid = requests.post(f'{URL}/client', json={'email': Faker().email()}).json()['wallet_uid']
# создаем ещё один кошелек
deposit_response = requests.post(f'{URL}/deposit', json={'wallet_uid': wallet_uid, 'amount': str(Decimal('1000')),
                                                         'request_uid': str(Faker().uuid4())})
# делаем депозит
print(deposit_response.json())

transfer_request_uid = str(Faker().uuid4())
request_data = {
    'request_uid': transfer_request_uid,
    'amount': str(Decimal('500')),
    'to_wallet_uid': another_wallet_uid,
    'from_wallet_uid': wallet_uid
}

for _ in range(2):
    transfer_response = requests.post(f'{URL}/transfer', json=request_data)
    # проверяем дважды, что запрос с одним request_uid невозможен
    print(transfer_response.json())

request_data['request_uid'] = str(Faker().uuid4())
request_data['amount'] = str(Decimal('600'))

transfer_response = requests.post(f'{URL}/transfer', json=request_data)
# проверяем, что перевод с кошелька с недостаточным балансом невозможен
print(transfer_response.json())

balance_response = requests.get(f'{URL}/balance/{wallet_uid}')
# запрашиваем баланс
print(balance_response.json())

# {'wallet_uid': '67eb1b86-3de4-487f-ab4d-bada95573664', 'balance': '1000.00', 'request_uid': 'd2e6294f-05c1-4f0c-a3ae-f0f1942edae3'} 200
# {'wallet_uid': '67eb1b86-3de4-487f-ab4d-bada95573664', 'balance': '500.00', 'request_uid': 'abb7d84b-ff04-4ec4-b6d3-d172f7b5625f'} 200
# {'message': 'Request uid should be unique', 'error_code': 'Request UUID not unique'} 400
# {'message': 'Wallet has not enough money for transfer', 'error_code': 'Not Enough Money'} 400
# {'wallet_uid': '9788543f-c4fe-4321-b47d-c309f117d209', 'balance': '500.00'} 200
```
