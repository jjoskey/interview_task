import random
from decimal import Decimal

import asynctest
import pytest
from faker import Faker

import config
from application.models.enums import OperationTypeEnum
from application.services.balance import BalanceController
from common.cache.basic import Cache
from tests.factories import WalletFactory, OperationFactory


async def test_set_balance(redis):
    wallet = await WalletFactory().create()
    balance_controller = BalanceController(wallet, redis)
    fake_balance = Faker().pydecimal(positive=True, left_digits=4, right_digits=2)
    initial_key_count = await redis.dbsize()

    await balance_controller.set_balance(fake_balance)

    cache = Cache(redis)
    result = await cache.get(f'balance_{wallet.uid}')

    assert initial_key_count == 0
    assert Decimal(result) == fake_balance


@pytest.mark.parametrize(
    'amount', [
        Faker().pydecimal(positive=True, left_digits=4, right_digits=2),
        0,
        Decimal('0')
    ]
)
async def test_get_balance_from_cache(redis, amount):
    wallet = await WalletFactory().create()
    balance_controller = BalanceController(wallet, redis)
    cache = Cache(redis)
    await cache.set(f'{config.BALANCE_CACHE_PREFIX}{wallet.uid}', amount)
    with asynctest.patch(
            'application.services.balance.BalanceController.get_balance_from_operations') as mocked_get_from_operations:
        balance = await balance_controller.get_balance()
    assert balance == amount
    mocked_get_from_operations.assert_not_awaited()


@pytest.mark.parametrize(
    'operations,expected_balance', [
        ([], 0),
        (
            [
                {'key': 'to_wallet', 'type_': OperationTypeEnum.deposit, 'amount': Decimal('20.05')},
                {'key': 'to_wallet', 'type_': OperationTypeEnum.transfer, 'amount': Decimal('19.1')},
                {'key': 'from_wallet', 'type_': OperationTypeEnum.transfer, 'amount': Decimal('7')}
            ],
            Decimal('32.15')
        ),
        (
            [
                {'key': 'to_wallet', 'type_': OperationTypeEnum.deposit, 'amount': Decimal('15.05')},
                {'key': 'to_wallet', 'type_': OperationTypeEnum.transfer, 'amount': Decimal('19')},
                {'key': 'from_wallet', 'type_': OperationTypeEnum.transfer, 'amount': Decimal('7')},
                {'key': 'to_wallet', 'type_': OperationTypeEnum.deposit, 'amount': Decimal('20.1')},
                {'key': 'from_wallet', 'type_': OperationTypeEnum.transfer, 'amount': Decimal('20.03')},
                {'key': 'to_wallet', 'type_': OperationTypeEnum.transfer, 'amount': Decimal('17.2')},
            ],
            Decimal('44.32')
        )
    ]
)
async def test_get_balance_from_operations(operations, expected_balance, redis):
    wallet = await WalletFactory().create()
    for operation in operations:
        operation[operation['key']] = wallet
        del operation['key']
        if operation['type_'] == OperationTypeEnum.deposit:
            operation['from_wallet'] = None
        await OperationFactory().create(**operation)
    for _ in range(random.randint(3, 10)):
        await OperationFactory().create()
    balance_controller = BalanceController(wallet, redis)
    cache = Cache(redis)
    balance = await balance_controller.get_balance_from_operations()
    assert balance == expected_balance
    assert Decimal(await cache.get(f'{config.BALANCE_CACHE_PREFIX}{wallet.uid}')) == expected_balance
