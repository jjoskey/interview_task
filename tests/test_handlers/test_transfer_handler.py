import asyncio
import itertools
import random
from decimal import Decimal

import asynctest
import pytest
from faker import Faker

import config
from application.models.db_models import Operation
from application.models.enums import StatusEnum, ErrorCodeEnum, OperationTypeEnum
from application.services.balance import BalanceController
from common.cache.basic import Cache
from common.db.basic import manager
from tests.factories import WalletFactory, OperationFactory


async def test_transfer(test_app, redis):
    payer_wallet = await WalletFactory().create()
    deposit_operation = await OperationFactory().create(
        to_wallet=payer_wallet, from_wallet=None, type_=OperationTypeEnum.deposit)
    payee_wallet = await WalletFactory().create()
    amount = Faker().pydecimal(
        right_digits=2, left_digits=3, positive=True, max_value=int(deposit_operation.amount - 1))
    request_uid = str(Faker().uuid4())
    request_data = {
        'to_wallet_uid': payee_wallet.uid,
        'from_wallet_uid': payer_wallet.uid,
        'amount': str(amount),
        'request_uid': request_uid
    }

    response = await test_app.post('/transfer', json=request_data)

    result = await response.json()
    operations_count = await manager.count(Operation.select())
    created_operation = await manager.get(Operation, from_wallet=payer_wallet)
    difference = deposit_operation.amount - amount
    cache = Cache(redis)
    payer_balance = Decimal(await cache.get(f'{config.BALANCE_CACHE_PREFIX}{payer_wallet.uid}'))
    payee_balance = Decimal(await cache.get(f'{config.BALANCE_CACHE_PREFIX}{payee_wallet.uid}'))

    assert response.status == StatusEnum.success.value
    assert result == {
        'wallet_uid': payer_wallet.uid,
        'balance': f'{difference:.2f}',
        'request_uid': request_uid
    }
    assert operations_count == 2
    assert payer_balance == difference
    assert payee_balance == amount
    assert created_operation.to_wallet == payee_wallet
    assert created_operation.amount == amount
    assert created_operation.type_ == OperationTypeEnum.transfer
    assert created_operation.request_uid == request_uid


async def test_transfer_concurrently(test_app):
    payer_wallet = await WalletFactory().create()
    await OperationFactory().create(
        to_wallet=payer_wallet, from_wallet=None, type_=OperationTypeEnum.deposit, amount=Decimal('1000'))
    payee_wallet = await WalletFactory().create()
    base_request_data = {
        'to_wallet_uid': payee_wallet.uid,
        'from_wallet_uid': payer_wallet.uid,
        'amount': str(Decimal('900'))
    }

    tasks, random_integer = [], random.randint(2, 9)
    for _ in range(random_integer):
        request_data = {'request_uid': str(Faker().uuid4())}
        request_data.update(base_request_data)
        task = asyncio.create_task(test_app.post('/transfer', json=request_data))
        tasks.append(task)

    responses = await asyncio.gather(*tasks)

    results = [await response.json() for response in responses]
    statuses = [response.status for response in responses]
    success_response_body = list(filter(lambda d: d.get('balance'), results))[0]
    success_status_count = statuses.count(StatusEnum.success.value)
    bad_request_response_count = results.count(
        {'message': 'Wallet has not enough money for transfer', 'error_code': ErrorCodeEnum.not_enough_money.value}
    )
    bad_request_status_count = statuses.count(StatusEnum.bad_request.value)
    operations_count = await manager.count(Operation.select())

    assert len(responses) == random_integer
    assert success_status_count == 1
    assert bad_request_response_count == bad_request_status_count == random_integer - 1
    assert operations_count == 2
    assert success_response_body == {
        'wallet_uid': payer_wallet.uid,
        'balance': '100.00',
        'request_uid': asynctest.mock.ANY
    }


async def test_transfer_concurrently_with_same_request_uid(test_app):
    payer_wallet = await WalletFactory().create()
    await OperationFactory().create(
        to_wallet=payer_wallet, from_wallet=None, type_=OperationTypeEnum.deposit, amount=Decimal('2000'))
    payee_wallet = await WalletFactory().create()
    request_uid = str(Faker().uuid4())
    request_data = {
        'to_wallet_uid': payee_wallet.uid,
        'from_wallet_uid': payer_wallet.uid,
        'amount': str(Decimal('500')),
        'request_uid': request_uid
    }

    tasks, random_integer = [], random.randint(2, 9)
    for _ in range(random_integer):
        task = asyncio.create_task(test_app.post('/transfer', json=request_data))
        tasks.append(task)

    responses = await asyncio.gather(*tasks)

    results = [await response.json() for response in responses]
    statuses = [response.status for response in responses]
    success_response_count = results.count(
        {'wallet_uid': payer_wallet.uid, 'balance': '1500.00', 'request_uid': request_uid})
    success_status_count = statuses.count(StatusEnum.success.value)
    bad_request_response_count = results.count(
        {'message': 'Request uid should be unique', 'error_code': ErrorCodeEnum.request_uid_not_unique.value}
    )
    bad_request_status_count = statuses.count(StatusEnum.bad_request.value)
    operations_count = await manager.count(Operation.select())
    created_operation = await manager.get(Operation, from_wallet=payer_wallet)

    assert len(responses) == random_integer
    assert success_response_count == success_status_count == 1
    assert bad_request_response_count == bad_request_status_count == random_integer - 1
    assert operations_count == 2
    assert created_operation.request_uid == request_uid


async def test_transfer_deadlock_not_happened(test_app, redis):
    wallets = []
    for _ in range(4):
        wallet = await WalletFactory().create()
        await OperationFactory().create(
            to_wallet=wallet, from_wallet=None, type_=OperationTypeEnum.deposit, amount=Decimal('1000'))
        wallets.append(wallet)
    tasks, amount = [], Decimal('10')
    for combination in itertools.combinations(wallets, 2):
        tasks.append(asyncio.create_task(test_app.post(
            '/transfer',
            json={'to_wallet_uid': combination[0].uid, 'from_wallet_uid': combination[1].uid,
                  'amount': str(amount), 'request_uid': str(Faker().uuid4())})))
        tasks.append(asyncio.create_task(test_app.post(
            '/transfer',
            json={'to_wallet_uid': combination[1].uid, 'from_wallet_uid': combination[0].uid,
                  'amount': str(amount), 'request_uid': str(Faker().uuid4())})))

    responses = await asyncio.gather(*tasks)
    operations_count = await manager.count(Operation.select())
    cache = Cache(redis)

    assert len(responses) == 12
    for response in responses:
        assert response.status == StatusEnum.success.value
    assert operations_count == 16
    for wallet in wallets:
        assert await cache.get(f'{config.BALANCE_CACHE_PREFIX}{wallet.uid}') == '1000.00'
        assert await BalanceController(wallet, redis).get_balance_from_operations() == Decimal('1000')


async def test_transfer_not_enough_money_error(test_app):
    payer_wallet = await WalletFactory().create()
    deposit_operation = await OperationFactory().create(
        to_wallet=payer_wallet, from_wallet=None, type_=OperationTypeEnum.deposit, amount=Decimal('500'))
    payee_wallet = await WalletFactory().create()
    request_data = {
        'to_wallet_uid': payee_wallet.uid,
        'from_wallet_uid': payer_wallet.uid,
        'amount': str(deposit_operation.amount + random.randint(1, 100)),
        'request_uid': str(Faker().uuid4())
    }

    response = await test_app.post('/transfer', json=request_data)

    result = await response.json()
    operations_count = await manager.count(Operation.select())

    assert response.status == StatusEnum.bad_request.value
    assert result == {
        'message': 'Wallet has not enough money for transfer',
        'error_code': 'Not Enough Money'
    }
    assert operations_count == 1


@pytest.mark.parametrize(
    'to_wallet_uid,from_wallet_uid,amount,request_uid', [
        (str(Faker().uuid4()), str(Faker().uuid4()),
         random.choice([Decimal('343.567'), Decimal('-500'), 0]), str(Faker().uuid4())),
        ('9fd40103-ee42-46ad-a8fb-975886e2510e', '9fd40103-ee42-46ad-a8fb-975886e2510e',
         Decimal('500'), str(Faker().uuid4())),
        ('9fd40103-ee42-46ad-a8fb-975886e2510e', '8fd40103-ee42-46ad-a8fb-975886e2510e',
         Decimal('500'), random.choice([Faker().pyint(), Faker().pystr()])),
    ]
)
async def test_transfer_validation_error(test_app, to_wallet_uid, from_wallet_uid, amount, request_uid):
    request_data = {
        'to_wallet_uid': to_wallet_uid,
        'from_wallet_uid': from_wallet_uid,
        'amount': str(amount),
        'request_uid': request_uid
    }

    response = await test_app.post('/transfer', json=request_data)

    result = await response.json()
    operation_count = await manager.count(Operation.select())

    assert response.status == StatusEnum.bad_request.value
    assert result == {
        'message': asynctest.mock.ANY,
        'error_code': ErrorCodeEnum.validation_error.value
    }
    assert operation_count == 0


@pytest.mark.parametrize(
    'key', ['to_wallet_uid', 'from_wallet_uid']
)
async def test_transfer_non_existent_wallet_error(test_app, key):
    wallet = await WalletFactory().create()
    request_data = {
        'from_wallet_uid': str(Faker().uuid4()),
        'to_wallet_uid': str(Faker().uuid4()),
        'amount': '500',
        'request_uid': str(Faker().uuid4())
    }
    request_data[key] = wallet.uid

    response = await test_app.post('/transfer', json=request_data)

    result = await response.json()
    operation_count = await manager.count(Operation.select())

    assert response.status == StatusEnum.not_found.value
    assert result == {
        'message': asynctest.mock.ANY,
        'error_code': ErrorCodeEnum.does_not_exist.value
    }
    assert operation_count == 0
