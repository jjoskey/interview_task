import asynctest
from faker import Faker

import config
from application.models.enums import StatusEnum, ErrorCodeEnum, OperationTypeEnum
from common.cache.basic import Cache
from tests.factories import WalletFactory, OperationFactory


async def test_balance_from_operations(test_app, redis):
    wallet = await WalletFactory().create()
    operation = await OperationFactory().create(to_wallet=wallet, type_=OperationTypeEnum.deposit)

    response = await test_app.get(f'/balance/{wallet.uid}')

    result = await response.json()
    cache = Cache(redis)
    balance_in_cache = await cache.get(f'{config.BALANCE_CACHE_PREFIX}{wallet.uid}')

    assert response.status == StatusEnum.success.value
    assert result == {
        'wallet_uid': wallet.uid,
        'balance': f'{operation.amount:.2f}'
    }
    assert balance_in_cache == f'{operation.amount:.2f}'


async def test_balance_from_cache(test_app, redis):
    wallet = await WalletFactory().create()
    cache = Cache(redis)
    amount = Faker().pydecimal(positive=True, left_digits=4, right_digits=2)
    await cache.set(f'{config.BALANCE_CACHE_PREFIX}{wallet.uid}', f'{amount:.2f}')

    with asynctest.patch(
            'application.services.balance.BalanceController.get_balance_from_operations') as mocked_get_from_operations:
        response = await test_app.get(f'/balance/{wallet.uid}')

    result = await response.json()

    assert response.status == StatusEnum.success.value
    assert result == {
        'wallet_uid': wallet.uid,
        'balance': f'{amount:.2f}'
    }
    mocked_get_from_operations.assert_not_awaited()


async def test_balance_non_existent_wallet_error(test_app):
    fake_wallet_uid = str(Faker().uuid4())

    response = await test_app.get(f'/balance/{fake_wallet_uid}')

    result = await response.json()

    assert response.status == StatusEnum.not_found.value
    assert result == {
        'message': asynctest.mock.ANY,
        'error_code': ErrorCodeEnum.does_not_exist.value
    }
