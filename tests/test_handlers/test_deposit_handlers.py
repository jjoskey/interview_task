import asyncio
import random
from decimal import Decimal

import asynctest
import pytest
from faker import Faker

from application.models.db_models import Operation
from application.models.enums import StatusEnum, ErrorCodeEnum, OperationTypeEnum
from common.db.basic import manager
from tests.factories import WalletFactory


async def test_deposit(test_app):
    wallet = await WalletFactory().create()
    amount = Faker().pydecimal(positive=True, left_digits=4, right_digits=2)
    request_uid = str(Faker().uuid4())
    request_data = {
        'wallet_uid': wallet.uid,
        'amount': str(amount),
        'request_uid': request_uid
    }

    response = await test_app.post('/deposit', json=request_data)

    result = await response.json()

    operation = await manager.get(Operation, to_wallet=wallet)
    operation_count = await manager.count(Operation.select())

    assert operation_count == 1
    assert operation.to_wallet == wallet
    assert operation.amount == amount
    assert operation.type_ == OperationTypeEnum.deposit
    assert operation.request_uid == request_uid
    assert response.status == StatusEnum.success.value
    assert result == {
        'wallet_uid': wallet.uid,
        'balance': f'{amount:.2f}',
        'request_uid': request_uid
    }


async def test_deposit_concurrently_with_same_request_uid(test_app):
    wallet = await WalletFactory().create()
    amount = Faker().pydecimal(positive=True, left_digits=4, right_digits=2)
    request_uid = str(Faker().uuid4())
    request_data = {
        'wallet_uid': wallet.uid,
        'amount': str(amount),
        'request_uid': request_uid
    }

    tasks, random_integer = [], random.randint(2, 9)
    for _ in range(random_integer):
        task = asyncio.create_task(test_app.post('/deposit', json=request_data))
        tasks.append(task)

    responses = await asyncio.gather(*tasks)

    results = [await response.json() for response in responses]
    statuses = [response.status for response in responses]
    success_response_count = results.count(
        {'wallet_uid': wallet.uid, 'balance': f'{amount:.2f}', 'request_uid': request_uid})
    success_status_count = statuses.count(StatusEnum.success.value)
    bad_request_response_count = results.count(
        {'message': 'Request uid should be unique', 'error_code': ErrorCodeEnum.request_uid_not_unique.value}
    )
    bad_request_status_count = statuses.count(StatusEnum.bad_request.value)
    operations_count = await manager.count(Operation.select())
    operation = await manager.get(Operation, to_wallet=wallet)

    assert len(responses) == random_integer
    assert success_response_count == success_status_count == 1
    assert bad_request_response_count == bad_request_status_count == random_integer - 1
    assert operations_count == 1
    assert operation.to_wallet == wallet
    assert operation.amount == amount
    assert operation.type_ == OperationTypeEnum.deposit
    assert operation.request_uid == request_uid


@pytest.mark.parametrize(
    'amount,request_uid', [
        (Decimal('343.567'), str(Faker().uuid4())),
        (Decimal('-500'), str(Faker().uuid4())),
        (0, str(Faker().uuid4())),
        (Decimal('500'), random.choice([Faker().pyint(), Faker().pystr()]))
    ]
)
async def test_deposit_validation_error(test_app, amount, request_uid):
    wallet = await WalletFactory().create()
    request_data = {
        'wallet_uid': wallet.uid,
        'amount': str(amount),
        'request_uid': str(request_uid)
    }

    response = await test_app.post('/deposit', json=request_data)

    result = await response.json()
    operation_count = await manager.count(Operation.select())

    assert response.status == StatusEnum.bad_request.value
    assert result == {
        'message': asynctest.mock.ANY,
        'error_code': ErrorCodeEnum.validation_error.value
    }
    assert operation_count == 0


async def test_deposit_non_existent_wallet_error(test_app):
    amount = Faker().pydecimal(positive=True, left_digits=4, right_digits=2)
    request_data = {
        'wallet_uid': str(Faker().uuid4()),
        'amount': str(amount),
        'request_uid': str(Faker().uuid4())
    }

    response = await test_app.post('/deposit', json=request_data)

    result = await response.json()
    operation_count = await manager.count(Operation.select())

    assert response.status == StatusEnum.not_found.value
    assert result == {
        'message': asynctest.mock.ANY,
        'error_code': ErrorCodeEnum.does_not_exist.value
    }
    assert operation_count == 0
