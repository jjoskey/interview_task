import asynctest
import pytest
from faker import Faker

from application.models.db_models import Client, Wallet
from application.models.enums import StatusEnum, ErrorCodeEnum
from common.db.basic import manager
from tests.factories import ClientFactory


async def test_client_create(test_app):
    email = Faker().email()

    response = await test_app.post('/client', json={'email': email})

    result = await response.json()
    client_count = await manager.count(Client.select())
    wallet_count = await manager.count(Wallet.select())
    client = await manager.get(Client, uid=result['client_uid'])
    wallet = await manager.get(Wallet, uid=result['wallet_uid'])

    assert client_count == wallet_count == 1
    assert wallet.client == client
    assert client.email == email
    assert result == {
        'client_uid': client.uid,
        'wallet_uid': wallet.uid
    }
    assert response.status == StatusEnum.success.value


@pytest.mark.parametrize(
    'wrong_email', [
        Faker().pystr(min_chars=1, max_chars=255),
        '',
    ]
)
async def test_client_create_validation_error(test_app, wrong_email):
    response = await test_app.post('/client', json={'email': wrong_email})

    result = await response.json()

    assert response.status == StatusEnum.bad_request.value
    assert result == {
        'message': asynctest.mock.ANY,
        'error_code': ErrorCodeEnum.validation_error.value
    }


async def test_client_create_already_exists_error(test_app):
    client = await ClientFactory().create()

    response = await test_app.post('/client', json={'email': client.email})

    result = await response.json()
    client_count = await manager.count(Client.select())

    assert client_count == 1
    assert response.status == StatusEnum.bad_request.value
    assert result == {
        'message': asynctest.mock.ANY,
        'error_code': ErrorCodeEnum.database_error.value
    }
