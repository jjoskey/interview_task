import random
from datetime import datetime

from faker import Faker

from application.models.db_models import Wallet, Client
from common.db.basic import manager
from tests.factories import WalletFactory


async def test_wallet_factory_from_scratch():
    wallet = await WalletFactory().create()

    wallet_from_db = await manager.get(Wallet, uid=wallet.uid)
    client_count = await manager.count(Client.select())
    wallet_count = await manager.count(Wallet.select())

    assert wallet_from_db == wallet
    assert client_count == wallet_count == 1


async def test_wallet_factory_with_kwargs():
    email = Faker().email()
    client = await manager.create(Client, **{'email': email})
    wallet_kwargs = {
        'uid': '2a038ecf-4d6d-4b2d-85f7-d2f15c0e3338',
        'created': datetime.now(),
        'client': client
    }
    wallet = await WalletFactory().create(**wallet_kwargs)
    wallet_from_db = await manager.get(Wallet, uid=wallet.uid)
    client_count = await manager.count(Client.select())
    wallet_count = await manager.count(Wallet.select())

    assert wallet_from_db == wallet
    assert client_count == wallet_count == 1
    assert wallet.uid == wallet_kwargs['uid']
    assert wallet.created == wallet_kwargs['created']
    assert wallet.client == wallet_kwargs['client']


async def test_wallet_factory_bulk():
    n = random.randint(2, 5)
    for _ in range(n):
        await WalletFactory().create()

    client_count = await manager.count(Client.select())
    wallet_count = await manager.count(Wallet.select())

    assert client_count == wallet_count == n
