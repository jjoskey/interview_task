import asyncio

import pytest

from application.models.db_models import Client, Wallet, Operation
from common.cache.basic import start as redis_start
from common.db.basic import start as db_start
from server import create_app


@pytest.fixture(scope='session', autouse=True)
def loop():
    yield asyncio.get_event_loop()


@pytest.fixture
async def test_app(aiohttp_client):
    app = create_app()
    yield await aiohttp_client(app)


@pytest.fixture(scope='session', autouse=True)
def init_db():
    db_start()


@pytest.fixture
async def redis():
    redis = await redis_start()
    yield redis
    redis.flushall()
    redis.close()
    await redis.wait_closed()


@pytest.fixture(autouse=True)
def clean_db():
    yield
    tables = (Client, Wallet, Operation)
    for table in tables:
        table.truncate_table(cascade=True)


@pytest.fixture(autouse=True)
async def clean_redis(redis):
    yield
    redis.flushall()
    redis.close()
    await redis.wait_closed()
