import inspect
import random

from faker import Faker

from application.models.db_models import Client, Wallet, Operation
from application.models.enums import OperationTypeEnum
from common.db.basic import manager


class AbstractFactory:
    model = NotImplemented

    async def create(self, *args, **kwargs):
        fields, values = self.model._meta.fields, dict()
        for field in fields:
            value = kwargs.get(
                field) if field in kwargs else getattr(self, field)
            if inspect.isclass(value) and issubclass(value, AbstractFactory):
                value = await value().create()
            values[field] = value
        return await manager.create(self.model, **values)


class BaseFactory(AbstractFactory):
    def __init__(self):
        self.uid = str(Faker().uuid4())
        self.created = Faker().date_time_this_month(before_now=True)


class ClientFactory(BaseFactory):
    model = Client

    def __init__(self):
        super().__init__()
        self.email = Faker().email()


class WalletFactory(BaseFactory):
    model = Wallet

    def __init__(self):
        super().__init__()
        self.client = ClientFactory


class OperationFactory(BaseFactory):
    model = Operation

    def __init__(self):
        super().__init__()
        self.to_wallet = WalletFactory
        self.from_wallet = WalletFactory
        self.amount = Faker().pydecimal(positive=True, left_digits=4, right_digits=2)
        self.type_ = random.choice([t for t in OperationTypeEnum])
        self.request_uid = str(Faker().uuid4())
