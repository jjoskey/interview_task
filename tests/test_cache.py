import pytest
from faker import Faker

from common.cache.basic import Cache


@pytest.mark.parametrize(
    'key,value', [
        (Faker().pystr(), Faker().pystr()),
        (Faker().pystr(), Faker().pydecimal()),
    ]
)
async def test_cache(redis, key, value):
    cache = Cache(redis)

    await cache.set(key, value)
    assert str(value) == await cache.get(key)
