from application.models.enums import ErrorCodeEnum, StatusEnum


class BaseAPIError(Exception):
    message = NotImplemented
    error_code = NotImplemented
    status = StatusEnum.bad_request.value


class NotEnoughMoneyError(BaseAPIError):
    message = 'Wallet has not enough money for transfer'
    error_code = ErrorCodeEnum.not_enough_money.value


class BadRequestUidError(BaseAPIError):
    message = 'Request uid should be unique'
    error_code = ErrorCodeEnum.request_uid_not_unique.value
