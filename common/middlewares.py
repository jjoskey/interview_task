import json
from functools import partial

from aiohttp import web
from aiohttp.web_response import Response
from peewee import DoesNotExist, IntegrityError
from pydantic import ValidationError

from application.models.enums import StatusEnum, ErrorCodeEnum
from application.models.response_models import ErrorResponse
from common import context
from common.errors import BaseAPIError

json_dumps = partial(json.dumps, ensure_ascii=False, default=str)


@web.middleware
async def create_rest_response(request: web.Request, handler: web.View) -> Response:
    try:
        result, status = await handler(request), StatusEnum.success.value
    except BaseAPIError as e:
        result = ErrorResponse(
            error_code=e.error_code,
            message=e.message
        )
        status = e.status
    except web.HTTPException as e:
        result = ErrorResponse(
            error_code=e.__class__.__name__,
            message=str(e.reason)
        )
        status = e.status_code
    except ValidationError as e:
        err_msg = f'Model: {e.model.__name__}, Error: {e.json()[:450]}'
        result = ErrorResponse(
            error_code=ErrorCodeEnum.validation_error.value,
            message=err_msg,
        )
        status = StatusEnum.bad_request.value
    except DoesNotExist as e:
        result = ErrorResponse(
            error_code=ErrorCodeEnum.does_not_exist.value,
            message=f'{e.__class__.__name__.split("DoesNotExist")[0]} has wrong uid'
        )
        status = StatusEnum.not_found.value
    except IntegrityError as e:
        result = ErrorResponse(
            message=str(e)[:500],
            error_code=ErrorCodeEnum.database_error.value
        )
        status = StatusEnum.bad_request.value
    except Exception as e:
        result = ErrorResponse(
            message=str(e)[:500],
            error_code=ErrorCodeEnum.internal_error.value
        )
        status = StatusEnum.server_error.value

    response = web.json_response(
        data=result.dict(), status=status, dumps=json_dumps)
    return response


@web.middleware
async def set_context(request: web.Request, handler: web.View):
    context.cache.set(request.app['redis'])
    return await handler(request)
