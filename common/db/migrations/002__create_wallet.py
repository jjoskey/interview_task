from yoyo import step

__depends__ = {'001__create_client'}
__transactional__ = False

steps = [
    step("""create table if not exists wallet (
                uid varchar(36) not null,
                created timestamp not null,
                client_id varchar(36) not null unique,
                constraint wallet_pkey
                    primary key (uid),
                constraint wallet_client_fk
                    foreign key (client_id) references client
    );"""),
    step("""create index concurrently if not exists wallet_client
    on wallet (client_id);"""),
    step("""create index concurrently if not exists wallet_uid
    on wallet (uid);"""),
]
