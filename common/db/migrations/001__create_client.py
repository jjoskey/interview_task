from yoyo import step

__depends__ = {}
__transactional__ = False

steps = [
    step("""create table if not exists client (
                uid varchar(36) not null,
                created timestamp not null,
                email varchar(255) not null unique,
                constraint client_pkey
                    primary key (uid)
    );"""),
    step("""create index concurrently if not exists client_uid
    on client (uid);"""),
    step("""create index concurrently if not exists client_email
    on client (email);"""),
]
