from yoyo import step

__depends__ = {'002__create_wallet'}
__transactional__ = False

steps = [
    step("""create table if not exists operation (
                uid varchar(36) not null,
                created timestamp not null,
                to_wallet_id varchar(36) not null,
                from_wallet_id varchar(36),
                amount numeric(20, 2) check (amount > 0),
                type_ smallint,
                request_uid varchar(36) not null unique,
                constraint operation_pkey
                    primary key (uid),
                constraint operation_to_wallet_fk
                    foreign key (to_wallet_id) references wallet,
                constraint operation_from_wallet_fk
                    foreign key (from_wallet_id) references wallet
    );"""),
    step("""create index concurrently if not exists operation_to_wallet
    on operation (to_wallet_id);"""),
    step("""create index concurrently if not exists operation_from_wallet
        on operation (from_wallet_id);"""),
]
