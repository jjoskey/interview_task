from datetime import datetime
from pathlib import Path

import peewee
import peewee_async
import yoyo
from playhouse.postgres_ext import Model, IntegerField

import config
from common.utils import uuid_str

database = peewee_async.PooledPostgresqlDatabase(None)
manager = peewee_async.Manager(database)


def start() -> peewee_async.Manager:
    database.init(
        database=config.DB_NAME,
        user=config.DB_USER,
        password=config.DB_PASS,
        host=config.DB_HOST,
        port=config.DB_PORT
    )
    database.connect()
    apply_migrations()
    return manager


def apply_migrations() -> None:
    dsn = f'postgres://{config.DB_USER}:{config.DB_PASS}@{config.DB_HOST}:{config.DB_PORT}/{config.DB_NAME}'
    backend = yoyo.get_backend(dsn, 'migrations')

    with backend.lock():
        migrations = yoyo.read_migrations(
            (Path(__file__).parent / 'migrations').as_posix())

        if not migrations:
            return
        backend.apply_migrations(backend.to_apply(migrations))


class BaseModel(Model):
    uid = peewee.CharField(max_length=36, unique=True,
                           primary_key=True, default=uuid_str)
    created = peewee.DateTimeField(default=datetime.now)

    class Meta:
        database = database


class EnumField(IntegerField):
    def __init__(self, enum, **kwargs):
        self._enum = enum
        self.value = None
        super().__init__(**kwargs)

    def db_value(self, value):
        assert isinstance(
            value, self._enum), f'Enum object {self._enum} expected, {type(value)} given'
        value = self._enum(value)
        return str(value.value)

    def python_value(self, value):
        return self._enum(value)
