from typing import Optional, Any

import aioredis

import config
from common import context


async def start() -> aioredis.Redis:
    redis = await aioredis.create_redis_pool((config.REDIS_HOST, config.REDIS_PORT))
    return redis


class Cache:
    def __init__(self, connect: Optional[aioredis.Redis] = None):
        self.redis = connect or context.cache.get()

    async def set(self, key: str, value: Any, expire: int = config.DEFAULT_EXPIRE) -> bool:
        return await self.redis.set(key, str(value), expire=expire)

    async def get(self, key: str) -> str:
        return await self.redis.get(key, encoding='UTF-8')
