from uuid import uuid4


def uuid_str() -> str:
    return str(uuid4())
